"use strict";
const service = require("../services/notifications");
const response = require("../exchange/response");


const dealOfTheDay = async (req, res) => {
    const log = req.context.logger.start("api:notifications:dealOfTheDAy");
    try {
        const notification = await service.dealOfTheDay(req.body, req.context);
        log.end();
        return response.data(res, notification);
        // return response.authorized(res, message, user, user.token);
    }
    catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }

};

const sendToAll = async (req, res) => {
    const log = req.context.logger.start("api:notifications:sendToAll");
    try {
        const notification = await service.sendToAll(req.body, req.context);
        log.end();
        return response.successmessage(res, notification);
    }
    catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }

};


exports.dealOfTheDay = dealOfTheDay;
exports.sendToAll = sendToAll;


