"use strict";
const service = require("../services/report");
const response = require("../exchange/response");

const getReport = async (req, res) => {
    const log = req.context.logger.start(`api:categories:getReport`);
    try {
        const report = await service.getReport(req.context);
        log.end();
        return response.data(res, report);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

exports.getReport = getReport;