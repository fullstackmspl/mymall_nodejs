module.exports = [
    {
        name: "sendToAll",
        properties: {
            title: {
                type: "string"
            },
            message: {
                type: "string"
            },
        }
    }
];