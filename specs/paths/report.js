module.exports = [

    {
        url: "/getReport",
        get: {
            summary: "Get Report",
            description: "Get Report",
            parameters: [],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    }
]