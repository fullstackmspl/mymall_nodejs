"use strict";

const getReport = async (context) => {
    const log = context.logger.start(`services:categories:allCategories`);
    let report = []
    const user = await db.user.find().count();
    const store = await db.store.find().count();
    const order = await db.order.find().count();
    report.push({ userCount: user })
    report.push({ storeCount: store })
    report.push({ orderCount: order })
    log.end();
    return report;

};

exports.getReport = getReport;
